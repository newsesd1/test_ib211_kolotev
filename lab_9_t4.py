import csv
coord = []

ident = int(input('using csv - 1\nusing txt - 2\n'))

if ident == 1:
    with open("output.csv", "r", newline="") as file:
        reader = csv.reader(file)
        for row in reader:
            x,y = int(row[0]), int(row[1])
            coord.append([x,y])
elif ident == 2:
    with open("output.txt", "r") as file:
        for row in file:
            x,y = map(int, row.split(" "))
            coord.append([x,y])
else:
    print("have no " + str(ident) + " argument")
print(coord)